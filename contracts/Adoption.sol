pragma solidity ^0.4.17-0.5.0;

/****************************************************

    https://solidity.readthedocs.io/en/develop/units-and-global-variables.html?special-variables-and-functions#special-variables-and-functions

    [ msg(메세지) 프로퍼티 ]

    msg.data : 호출 데이터. bytes.
    msg.sender : 스마트 계약을 호출한 이더리움 주소 혹은 계정. address.
    msg.value : 스마트 계약 주소가 가진 이더량. uint.
    msg.gas : gas limit - gas. 스마트 계약 내 로직을 실행하고 남은 가스. uint
    * gas limit이란? : 가스 제한량. 내가 작성한 스마트 계약이 EVM에서 구동될 때 로직 연산에 대한 수수료(gas)를 납부하게 되는데,
                      수수료 과다 납부에 의해 이더량이 바닥나는걸 방지하기 위해 작성자가 사전에 제한을 걸 수 있다.
                      제한량을 높일 수록 EVM 내에서 우선 순위에 올라가게 된다. 빠른 연산을 원한다면 돈을 더 내라는 의미.

    [ address타입 변수의 사용 ]

    <address>.balance : <address>에 있는 wei의 잔액. uint256.
    <address payable>.transfer(uint256 amount) : 주어진 amount만큼의 wei를 <address>로 보낸다.
    <address payable>.send(uint256 amount) returns (bool) : 주어진 amount만큼의 wei를 <address>로 보낸다. 실패시 false 반환.
    * payable이란? : function을 작성할 때 넣는 키워드. 스마트 계약 주소와 외부 주소간 이더 전송을 받을 수 있도록 해준다.
                    즉 payable 키워드가 달린 메소드 내에서만 transfer/send 사용 가능.

****************************************************/

contract Adoption {
    
    address[16] public adopters; // public 변수의 경우 자동으로 getter 메소드를 가지게 된다.
                                 // 다만 지금 처럼 변수가 배열이라면 전체 배열이 아니라 특정 인덱스의 값을 가져오는 getter 메소드가 생성된다.

    // 입양
    function adopt(uint petId) public returns (uint) { // 솔리디티에서는 메소드 매개 변수와 출력 유형을 모두 지정해야 한다.
        require(petId >= 0 && petId <= 15); // require(bool). false일 경우 revert 발생.

        adopters[petId] = msg.sender;

        return petId;
    }

    // 입양된 펫 소유자 가져오기
    function getAdopters() public view returns (address[16] memory) { // view 키워드는 읽기 전용 메소드로 지정할 때 사용한다. 수수료를 절약 할 수 있다.
                                                                      // memory 속성은 솔리디티가 스마트 계약의 storage 대신
                                                                      // 메모리에 값을 일시적으로 저장하겠다는 의미이다. 역시 수수료를 절약 할 수 있다.
        return adopters;
    }

}